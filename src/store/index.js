import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loadedMeetups: [{
      imageUrl: 'https://www.choicehotels.com/cms/images/choice-hotels/demand-articles/know-before-you-go-new-york-city-manhattan/know-before-you-go-new-york-city-manhattan.jpg',
      id: 'dgfasdfasd',
      title: 'Meetup in New York',
      date: '2017-07-17'
    },
    {
      imageUrl: 'http://www.telegraph.co.uk/content/dam/Travel/Destinations/Europe/Russia/Moscow/moscow-attractions-red-square2-xlarge.jpg',
      id: '32f23fzxc',
      title: 'Meetup in Moscow',
      date: '2017-07-19'
    }
    ],
    user: {
      id: 'asdgasdg',
      registerMeetups: ['sfghsdfgfdg']
    }
  },
  mutations: {},
  actions: {},
  getter: {
    loadedMeetups (state) {
      return state.loadedMeetups.sort((meetupA, meetupB) => {
        return meetupA.date > meetupB.date
      })
    },
    featuredMeetups (state, getters) {
      return getters.loadedMeetups.slice(0, 5)
    },
    loadedMeetup (state) {
      return (meetupId) => {
        return state.loadedMeetups.find((meetup) => {
          return meetup.id === meetupId
        })
      }
    }
  }
})
